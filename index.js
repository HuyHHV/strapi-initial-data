const _ = require('lodash')
const VError = require('verror')
const fs = require('fs')
const csv = require('csvtojson')

module.exports = async function loadModelData(strapi) {
  const start = Date.now()

  await initImages()

  const forceInitFirst = [
    // Init certain LUTs first as they are required for associations
    'api::protocol.protocol',
    'api::project.project',

    // Plot location dependant luts - Remove
    'api::lut-state.lut-state',

    // CWD LUTs dependencies `lut-cwd-method` - Keep
    'api::lut-cwd-sampling-survey-method.lut-cwd-sampling-survey-method',
    'api::lut-cwd-transect-number.lut-cwd-transect-number',

    // Observations Models - Keep (Corner Case)
    'api::lut-tier-1-observation-method.lut-tier-1-observation-method',
    'api::lut-tier-2-observation-method.lut-tier-2-observation-method',
    'api::lut-tier-3-observation-methods.lut-tier-3-observation-methods',
    'api::lut-tier-4-observation-method.lut-tier-4-observation-method',
    'api::lut-invertebrate-post-field-guideline-group-tier-1.lut-invertebrate-post-field-guideline-group-tier-1',
    'api::lut-invertebrate-post-field-guideline-group-tier-2.lut-invertebrate-post-field-guideline-group-tier-2',

    //  Dependencies for `Lut-targeted-flora-survey`
    'api::lut-veg-height-class.lut-veg-height-class',
    'api::lut-veg-structural-formation.lut-veg-structural-formation',
    'api::lut-targeted-fauna-taxa-type.lut-targeted-fauna-taxa-type',
  ]

  for (const apiName of forceInitFirst) {
    await initModel(apiName, strapi.contentTypes, strapi)
  }

  // TODO use a graph to sort and init models
  // Bandaid Fix
  // Separating each set of models into their own arrays
  var lutOrderArray = []
  var protocolOrderArray = []
  // Reserved for combining all arrays
  var modelOrderArray = []

  // Adding Edge Cases which need to be created first/ in a specific order
  const forceInitOtherModelsFirst = [
    'api::plot-location.plot-location',
    'api::plot-visit.plot-visit',
    'api::plot-layout.plot-layout',

    //protocol init data bandaid fix - photopoints and opportune don't need to be forced
    //  as they're stand-alone models that aren't depended on by other models
    //need bird survey for bird survey obs
    'api::bird-survey.bird-survey',

    //need vouchers (and survey) for PI and plant tissue vouchering
    'api::floristics-veg-survey-full.floristics-veg-survey-full',
    'api::floristics-veg-voucher-full.floristics-veg-voucher-full',

    //need survey and species intercepts (child ob) so ob (points) works
    'api::cover-point-intercept-survey.cover-point-intercept-survey',
    'api::cover-point-intercept-species-intercept.cover-point-intercept-species-intercept',

    //need survey of plant tissue vouchering
    'api::floristics-veg-genetic-voucher-survey.floristics-veg-genetic-voucher-survey',

    //need survey and child obs (species cover and substrate cover) for ob to work
    'api::vegetation-mapping-survey.vegetation-mapping-survey',
    'api::vegetation-mapping-species-cover.vegetation-mapping-species-cover',
    'api::vegetation-mapping-substrate-cover.vegetation-mapping-substrate-cover',
  ]

  for (const apiName of forceInitOtherModelsFirst) {
    protocolOrderArray.push(apiName)
  }

  // Looping through all the models and putting them into their correct array
  for (const currModelName of Object.keys(strapi.contentTypes).sort()) {
    // LUTs
    if (currModelName.includes('api::lut-')) {
      lutOrderArray.push(currModelName)
    }
    // Everything Else - only if wasn't forced first
    else if (!forceInitFirst.includes(currModelName)) {
      // Skipping `plot-layout` removes duplicated data when fresh reloading 
      // hot-reloading still dupes the plot-layout data - this might have something to do with 'repeatable components'
      if (currModelName.includes('plot-layout')) {
        console.log("Skipping ", currModelName, " from the Queue")
      } else {
        protocolOrderArray.push(currModelName)
      }
    }
  }

  modelOrderArray = lutOrderArray.concat(protocolOrderArray)

  for (const currModelName of modelOrderArray) {
    // console.log("------currModelName = " + JSON.stringify(currModelName));
    const currModel = strapi.contentTypes[currModelName]
    // console.log("------currModel = " + JSON.stringify(currModel));
    // console.log("------currModelName = " + JSON.stringify(currModelName));
    let data = currModel.initialData
    if (currModel.initialData == undefined) {
      //console.log("skipping - no initialData for contentType: " + currModelName);
      continue
    }
    //console.log("currModel.initialData: " + currModel.initialData);
    if (!data) {
      // Load from a separate datafile, if the init data is not bundled with the model
      let filePath = `${__dirname}/../../api/` + currModelName + '/models/initData.datafile'
      let filePathCSV = `${__dirname}/../../api/` + currModelName + '/models/' + currModelName + '.csv'
      if (fs.existsSync(filePath)) {
        // Try the .data file format
        let rawdata = fs.readFileSync(filePath)
        data = JSON.parse(rawdata).rawData
        //strapi.log.info('Loaded ' + currModelName + ' initData from explicit V1-Formatted datafile');
      } else if (fs.existsSync(filePathCSV)) {
        // Try the .csv file format
        //strapi.log.info('Detected V2 (CSV) fall-through data file input: ' + JSON.stringify(filePath));
        data = await csv({
          noheader: true,
          output: 'csv'
        }).fromFile(filePathCSV)
        //strapi.log.info('Loaded ' + currModelName + ' initData from explicit V2-CSV datafile');
        //strapi.log.info('Resulting data ' + JSON.stringify(data));
      } else {
        continue
      }
    }
    const colNames = Object.keys(currModel.attributes).reduce((accum, currKey) => {
      if (!currModel.attributes[currKey].private) {
        accum.push(currKey)
      }
      return accum
    }, [])
    //strapi.log.info("   Column Names : " + JSON.stringify(colNames));
    //strapi.log.info("   Using data:    " + JSON.stringify(data));
    await processModelData(currModelName, colNames, data, strapi)
  }
  strapi.log.info(`Took ${Date.now() - start}ms to load all initial data`)
}

async function initModel(modelName, contentTypes, strapi) {
  const model = contentTypes[modelName]
  //strapi.log.info(`initModel: model is: ` + JSON.stringify(modelName));
  //sometimes model that is specified may not exist. If this isn't handled, all
  //proceeding initial data will fail
  if (model) {
    const colNames = Object.keys(model.attributes).reduce((accum, currKey) => {
      if (!model.attributes[currKey].private) {
        accum.push(currKey)
      }
      return accum
    }, [])
    const data = model.initialData
    await processModelData(modelName, colNames, data, strapi)
  }
}

async function processModelData(modelName, colNames, rawData, strapi) {
  //strapi.log.info("modelName:    " + JSON.stringify(modelName));
  const uniqueColName = colNames[0]
  //strapi.log.info("uniqueColName:    " + JSON.stringify(uniqueColName));

  const data = rawData.map((e) => _.zipObject(colNames, e))
  //strapi.log.info(`data row: ${data}:` + JSON.stringify(data, null, 2));
  //const orm = await strapi.db.query(modelName);
  // FIXME - is a limit of 500 OK?
  //const existingRecords = await orm.findMany({ limit: 500, orderBy: { [uniqueColName]: "asc" } });
  const existingRecords = await strapi.entityService.findMany(modelName, {
    populate: getPopulateFromSchema(strapi.getModel(modelName)),
    limit: 500
  })
  //strapi.log.info("existingRecords = " + JSON.stringify(existingRecords));
  let total = 0
  let updated = 0
  let created = 0
  for (const curr of data) {
    //strapi.log.info("curr = " + JSON.stringify(curr));
    // if (typeof curr === 'object') {
    //   strapi.log.info("skipping Object compare, returning true");
    //   return (true);
    // }
    total += 1
    const found = await existingRecords.find((e) => {
      //strapi.log.info(`1.) inside find callback, checking on "${uniqueColName}"`);
      //strapi.log.info(`1a) inside find callback, curr is: ` + JSON.stringify(curr, null, 2));
      //strapi.log.info(`1b) inside find callback, e is: ` + JSON.stringify(e, null, 2));
      //strapi.log.info(`2) inside find callback, trying to match it with the value: ` + JSON.stringify(e[uniqueColName], null, 2));
      //strapi.log.info(`3) inside find callback, curr[uniqueColName] value is: ` + JSON.stringify(curr[uniqueColName], null, 2));
      //strapi.log.info(`4) returning boolean: ${e[uniqueColName] == curr[uniqueColName]}`);

      let mappedCol
      if (typeof e[uniqueColName] === 'object') {
        mappedCol = {}
        for (let key of Object.keys(e[uniqueColName])) {
          if (typeof e[uniqueColName][key] === 'object') {
            if (e[uniqueColName][key]) {
              mappedCol[key] = e[uniqueColName][key].id
            } else {
              mappedCol[key] = null
            }
          } else {
            mappedCol[key] = e[uniqueColName][key]
          }
        }
        if (mappedCol.id) delete mappedCol.id
      } else {
        mappedCol = e[uniqueColName]
      }

      return _.isEqual(mappedCol, curr[uniqueColName])
    })
    //strapi.log.info(`After check for existing records, got result: ` + JSON.stringify(found, null, 2) + `: for equality...`);
    // FIXME cannot handle models that have draftAndPublish=true. To add
    // support, we probably need to provide the `published_at` field set to now.
    try {
      if (found) {
        const isChangeRequired = Object.keys(curr).some(function callbackFn(k, index) {
          //strapi.log.info(`checking: k:` + k + `, index:` + index);

          if (curr[k] == 0 && found[k] == {}) {
            // trap the NULL vs empty object case
            return false
          } else if (curr[k] == 'true' && found[k] == true) {
            // trap the string true vs boolean true case
            return false
          } else if (curr[k] == 'false' && found[k] == false) {
            // trap the string true vs boolean false case
            return false
          }
          //strapi.log.info(`k: ` + k);
          //strapi.log.info(`curr[k]: ` + JSON.stringify(curr[k]));
          //strapi.log.info(`curr[k] typeof: ` + typeof curr[k]);
          //if (curr[k]) strapi.log.info(`curr[k] Object.entries(curr[k]).toString(): ` + Object.entries(curr[k]).toString());
          //strapi.log.info(`found[k]: ` + JSON.stringify(found[k]));
          //strapi.log.info(`found[k] typeof: ` + typeof found[k]);
          //if (found[k]) strapi.log.info(`found[k] Object.entries(found[k]).toString(): ` + Object.entries(found[k]).toString());

          if (found[k] == undefined || k == 'createdAt' || k == 'updatedAt') {
            //strapi.log.info(`detected undefined/irrelevant element to compare with: returning false`);
            return false
          }
          // Convert to String for comparison

          if (k == 'version') {
            //strapi.log.info(`Handling version...`);

            if (JSON.stringify(curr[k]) == JSON.stringify(found[k])) {
              return true
            } else {
              return false
            }
          }
          if (!_.isEqual(curr[k], found[k])) {
            //strapi.log.info(`Detected a difference at element (` + k + `): ` + JSON.stringify(curr[k]));
            //strapi.log.info(`curr[k] !== found[k]: ` + JSON.stringify(curr[k]) + "!= " + JSON.stringify(found[k]));
            return true
          }
        })
        if (!isChangeRequired) {
          continue
        }
        //await orm.update({ where: { id: found.id }, data: Object.assign(found, curr) });
        const entry = await strapi.entityService.update(modelName, found.id, {
          data: Object.assign(found, curr)
        })
        //strapi.log.info(`Just performed an ORM update with:` + JSON.stringify(curr));
        updated += 1
        continue
      } else {
        // Not found - why?
        //strapi.log.info(`No match on ${modelName}: could not find a record with: ${curr[uniqueColName]}`);
        //await orm.create({ data: curr });
        const entry = await strapi.entityService.create(modelName, {
          data: curr
        })
        created += 1
      }
    } catch (err) {
      const newErr = new VError(err, `Error while processing data=${JSON.stringify(curr)} for model=${modelName}`)
      console.trace()
      throw newErr
    }
  }
  strapi.log.info(`Initial data for ${modelName}: created=${created}, updated=${updated}, ` + `total=${total}`)
}

function populateAttribute({ components }) {
  if (components) {
    const populate = components.reduce((currentValue, current) => {
      let data = { ...currentValue, [current.split('.').pop()]: { populate: '*' } }
      return data
    }, {})
    return { populate }
  }
  return { populate: '*' }
}

function getPopulateFromSchema(schema) {
  return Object.keys(schema.attributes).reduce((currentValue, current) => {
    const attribute = schema.attributes[current]
    if (!['dynamiczone', 'component', 'relation'].includes(attribute.type)) {
      return currentValue
    }
    return {
      ...currentValue,
      [current]: populateAttribute(attribute)
    }
  }, {})
}

//https://forum.strapi.io/t/upload-buffer-using-strapi-upload/18807/2
async function initImages() {
  const { Readable } = require('stream')
  const getServiceUpload = (name) => {
    return strapi.plugin('upload').service(name)
  }
  const uploadAndLinkDocument = async (buffer, { filename, extension, mimeType}) => {
    const config = strapi.config.get('plugin.upload')

    // add generated document
    const entity = {
      name: filename,
      alternateText: filename,
      caption: filename,
      width: 0, //FIXME
      height: 0,  //FIXME
      hash: filename,
      ext: extension,
      mime: mimeType,
      size: buffer.length,
      provider: config.provider,
    }
    entity.getStream = () => Readable.from(buffer)
    await getServiceUpload('provider').upload(entity)
    const fileValues = { ...entity }
    const res = await strapi
      .query('plugin::upload.file')
      .create({ data: fileValues })
    return res
  }
  //https://stackoverflow.com/a/2727191
  const initImagesPath = './node_modules/strapi-initial-data/init_images'
  const fileNames = fs.readdirSync(initImagesPath)
  for (const fileName of fileNames) {
    if(fileName === 'README.md') continue
    strapi.log.info(`Initialising file with name '${fileName}'`)
    const file = fs.readFileSync(`${initImagesPath}/${fileName}`,
      function (err, data) {
        if (err) throw err
        console.log('file data: ', data)
      }
    )
    await uploadAndLinkDocument(file, {
      filename: fileName,
      extension: '.jpg',
      mimeType: 'image/jpeg'
    })
  }
}