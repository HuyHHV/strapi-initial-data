> util to upsert model data on Strapi.io bootstrap

**Note**: this util is still in beta. It works for our use-case but may not work
for others.

# Assumptions

 - the first field listed in a model definition's `attributes` is the key (and
   unique)
 - `draftAndPublish` is set to `false`. Support for this is possible, it just
   isn't implemented yet

# Why
We have strapi models that are look-up tables. They have a fixed set of records
in them that rarely changes. We want the values for the table to exist as code
and every time the server starts, it makes sure the DB table is up-to-date.

# Example
See the [example/](./example/) directory for a full strapi project that uses
this util.

# Usage
 1. install the util
    ```bash
    yarn add "strapi-initial-data@https://gitlab.com/ternandsparrow/strapi-initial-data.git#main"
    ```
 1. call the util in the `config/functions/bootstrap.js` script:
    ```js
    const strapiInitialData = require('strapi-initial-data')

    module.exports = () => {
      // if you don't need data load to block the server start, don't await it
      // you must pass the `strapi` global to the function
      strapiInitialData(strapi).catch(err => {
        // using console over strapi.log because strapi doesn't handle errors well
        console.error('Failed to load all initial data', err)
      })
    }
    ```
 1. for any models that you want initial data for, edit the `.settings.json`
    file for that model, and add an `initialData` key. The value is a 2D array
    with each "row" consisting of values in the same order that they appear in
    the `attributes` key.
    ```js
    {
      ...
      "options": {
        ...
        "draftAndPublish": false  // currently we don't support drafts
      },
      "attributes": {
        "code": {                 // first attribute must be unique
          "type": "string",
          "required": true,
          "unique": true
        },
        "Name": {
          "type": "string"
        }
      },
      "initialData": [            // this is the key to add
        ["MAM", "Mammals"],       // order of values matches attributes key above
        ["BRD", "Birds"]
      ]
    }
    ```
 1. start the strapi server and you'll see a report outlining records loaded:
    ```
    ...
    [2021-02-02T04:30:21.003Z] info Initial data for animal-type: created=2, updated=0, total=2
    [2021-02-02T04:30:21.004Z] info Took 53ms to load all initial data
    ...
    ```
